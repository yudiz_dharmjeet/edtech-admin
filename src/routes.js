import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import DashboardLayout from './layouts/dashboard';
import LogoOnlyLayout from './layouts/LogoOnlyLayout';
//
// import Blog from './pages/Blog';
// import User from './pages/User';
import Login from './pages/Login';
import NotFound from './pages/Page404';
// import Register from './pages/Register';
// import Products from './pages/Products';
import DashboardApp from './pages/DashboardApp';
import PrivateRoute from './routes/PrivateRoute';
import PublicRoute from './routes/PublicRoute';
import Subject from './pages/Subject';
import AddSubject from './pages/AddSubject';
import Topic from './pages/Topic';
import AddTopic from './pages/AddTopic';
// import User from './pages/User';
// import Blog from './pages/Blog';

// ----------------------------------------------------------------------

export default function Router() {
  return useRoutes([
    {
      path: '/dashboard',
      element: <DashboardLayout />,
      children: [
        { path: 'app', element: <PrivateRoute element=<DashboardApp /> /> },
        { path: 'subject', element: <PrivateRoute element=<Subject /> /> },
        { path: 'subject/add', element: <PrivateRoute element=<AddSubject /> />},
        { path: 'topic', element: <PrivateRoute element=<Topic /> /> },
        { path: 'topic/add', element: <PrivateRoute element=<AddTopic /> />},
        // { path: 'user', element: <PrivateRoute element=<User /> /> },
        // { path: 'products', element: <PrivateRoute element=<Products /> /> },
        // { path: 'blog', element: <PrivateRoute element=<Blog /> /> },
      ],
    },
    {
      path: 'login',
      element: <PublicRoute element=<Login /> />,
    },
    // {
    //   path: 'register',
    //   element: <PublicRoute element=<Register /> />,
    // },
    {
      path: '/',
      element: <LogoOnlyLayout />,
      children: [
        { path: '/', element: <Navigate to="/dashboard/app" /> },
        { path: '404', element: <NotFound /> },
        { path: '*', element: <Navigate to="/404" /> },
      ],
    },
    {
      path: '*',
      element: <Navigate to="/404" replace />,
    },
  ]);
}
