import { LoadingButton } from '@mui/lab'
import { FormControl, Input, InputLabel } from '@mui/material'
import { useMutation } from '@tanstack/react-query'
import React from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import axios from 'src/config/axios'

function AddSubject() {
  const navigate = useNavigate()

  const [subject, setSubject] = useState('')

  const mutation = useMutation(newSubject => {
    return axios.post('/subject/add', newSubject)
  })

  function handleChange (e) {
    setSubject(e.target.value)
  }

  function handleSubmit () {
    mutation.mutate({ sName: subject })
    navigate('/dashboard/subject')
  }

  return (
    <>
      <FormControl>
        <InputLabel htmlFor="my-input">Subject</InputLabel>
        <Input id="my-input" aria-describedby="my-helper-text" onChange={(e) => handleChange(e)} />
      </FormControl>

      <LoadingButton size="large" type="submit" variant="contained" sx={{ mx: 3 }} onClick={handleSubmit}>
        Add
      </LoadingButton>
    </>
  )
}

export default AddSubject