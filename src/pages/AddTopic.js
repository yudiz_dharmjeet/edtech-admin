import React, { useEffect, useRef, useState } from 'react'
import { Button, TextField } from '@mui/material'
import { Editor } from '@tinymce/tinymce-react';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import { useQuery } from '@tanstack/react-query';
import axios from '../config/axios';
const animatedComponents = makeAnimated()

async function fetchSubjects(page, rowsPerPage, order, filterName) {
  const { data } = await axios.get(`/subject/list?nPage=${page}&nLimit=${rowsPerPage}&sOrder=${order}&sSearch=${filterName}`)
  return data
}

function AddTopic() {
  const editorRef = useRef(null);

  const [page, setPage] = useState(0);
  const [options, setOptions] = useState([])
  const [topicName, settopicName] = useState('')
  const [subjectActivePage, setSubjectActivePage] = useState(1)

  const { isLoading, data } = useQuery(['subjects', page],() => fetchSubjects(page, 10, 'asc', ''))

  useEffect(() => {
    if (data?.data?.data) {
      const arr = []
      data?.data?.data?.map((item) => {
        const obj = {
          value: item._id,
          label: item.sName
        }
        arr.push(obj)
        return obj
      })
      setOptions([...options, ...arr])
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.data?.data])

  function onHandleChange(selected) {
    settopicName(selected)
  }

  function onSubjectPagination() {
    const length = Math.ceil(options?.total / 10)
    if (subjectActivePage < length) {
      const start = subjectActivePage * 10
      setPage(start)
      setSubjectActivePage(subjectActivePage + 1)
    }
  }

  if (isLoading) return <h1>Loading...</h1>
  
  return (
    <>
      <h1>Add Topic</h1>
      <form style={{width: 600, marginTop: '20px'}}>
          <TextField
            style={{ width: "600px", margin: "5px" }}
            type="text"
            label="Name"
            variant="outlined"
          />
          <br />
          <br />
          <div style={{ marginBottom: '50px'}}>
            <Editor
              onInit={(evt, editor) => editorRef.current = editor}
              initialValue=""
              init={{
                height: 300,
                menubar: false,
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | ' +
                'removeformat | help',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
              }}
            />
          </div>
          <Select 
            options={options}
            menuPlacement="auto"
            menuPosition="fixed"
            captureMenuScroll={true}
            closeMenuOnSelect={false}
            components={animatedComponents}
            id="topicName"
            name="topicName"
            placeholder="Select Subject"
            value={topicName}
            onMenuScrollToBottom={onSubjectPagination}
            onChange={selected => onHandleChange(selected)}
            style={{ width: '380px', margin: '5px' }}
          />
          <Button variant="contained" color="primary" sx={{my: 3}}>
            Add Topic
          </Button>
        </form>
      </>
  )
}

export default AddTopic