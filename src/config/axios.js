import axios from "axios";

export default axios.create({
  baseURL: "https://a64d-103-156-142-125.in.ngrok.io/api/v1",
  headers: {
    "Content-type": "application/json"
  }
});